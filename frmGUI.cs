using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace AbstractsANDInterfaces
{
	/// <summary>
	/// Summary description for user interface.
	/// </summary>
	public class FormAbstractVSInterface : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnInterfaceExample;
		private System.Windows.Forms.Button btnAbstractExample;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormAbstractVSInterface()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.btnInterfaceExample = new System.Windows.Forms.Button();
            this.btnAbstractExample = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnInterfaceExample
            // 
            this.btnInterfaceExample.Location = new System.Drawing.Point(72, 138);
            this.btnInterfaceExample.Name = "btnInterfaceExample";
            this.btnInterfaceExample.Size = new System.Drawing.Size(136, 24);
            this.btnInterfaceExample.TabIndex = 0;
            this.btnInterfaceExample.Text = "Interface Example";
            this.btnInterfaceExample.Click += new System.EventHandler(this.btnInterfaceExample_Click);
            // 
            // btnAbstractExample
            // 
            this.btnAbstractExample.Location = new System.Drawing.Point(72, 80);
            this.btnAbstractExample.Name = "btnAbstractExample";
            this.btnAbstractExample.Size = new System.Drawing.Size(136, 24);
            this.btnAbstractExample.TabIndex = 1;
            this.btnAbstractExample.Text = "Abstract Example";
            this.btnAbstractExample.Click += new System.EventHandler(this.btnAbstractExample_Click);
            // 
            // FormAbstractVSInterface
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(292, 265);
            this.Controls.Add(this.btnAbstractExample);
            this.Controls.Add(this.btnInterfaceExample);
            this.Name = "FormAbstractVSInterface";
            this.Text = "Abstracts vs Interfaces";
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormAbstractVSInterface());
		}
		
		//In this sub i create an arraylist.
		//then create a fulltime and assign it to IEmpoloyee
		//then set the properties ...
		//and then adding the empolyee in the arraylist
		//then repeating the same process for the contractor employee
		//finally looping through the arraylist to call the calculate wage
		//method of the objects without knowing the type of the objects!
		private void btnInterfaceExample_Click(object sender, System.EventArgs e)
		{
			try
			{

				IEmployee emp;
                Emp_fulltime_I emp2 = new Emp_fulltime_I();
                //Emp_Fulltime_AI emp2 = new Emp_Fulltime_AI(); // With this it uses the abstrac class methods --hjoab
                //has to be casted because of the interface!
                emp = (IEmployee) emp2;
				emp.ID = "2234";
				emp.FirstName= "Rahman" ;
				emp.LastName = "Mahmoodi" ;
				//call add method of the object
				MessageBox.Show(emp.Add().ToString());
				
				//call the CalculateWage method
				MessageBox.Show(emp.CalculateWage().ToString());


			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			}

		private void btnAbstractExample_Click(object sender, System.EventArgs e)
		{

			AEmployee emp;
			//no casting is requird!
		    emp = new Emp_Fulltime_AI();

            emp.ID = "2244";
            emp.FirstName = "Maria";
            emp.LastName = "Robinlius";
            MessageBox.Show(emp.Add().ToString());

			//call the CalculateWage method
			MessageBox.Show(emp.CalculateWage().ToString());

		}


	}
}
